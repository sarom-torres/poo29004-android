package engtelecom.poo.sarom.meuprimeiroapp;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private int mContador =0; //Quando  a variavel pode ser m idifcada pela convenção poe m na frente;
    private TextView mTextViewContador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextViewContador = findViewById(R.id.tvContador);

        if(savedInstanceState != null){

            mContador = savedInstanceState.getInt("cont");
            if(mTextViewContador != null){
                mTextViewContador.setText(mContador);
            }
        }
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //Salvar o valor da variável mcontador
        outState.putInt("cont",mContador);
    }

    public void incrementarContador(View view) {

        mContador++;
        mTextViewContador.setText(Integer.toString(mContador));

    }

    public void exibirMensagem(View view) {
//        Toast toast = Toast.makeText(this,R.string.texto_saudacao,Toast.LENGTH_LONG);
//        toast.show();


//        Uri uri = Uri.parse("http://docente.ifsc.edu.br");
//        Intent it = new Intent(Intent.ACTION_VIEW, uri);
//        startActivity(it);
//
//        Uri uri = Uri.parse("tel:33812800");
//        Intent it = new Intent(Intent.ACTION_DIAL, uri);
//        startActivity(it);

        Intent messageIntent = new Intent(this,SegundaActivity.class);
        startActivity(messageIntent);
    }
}
